{
  description = "OSY pages flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-24.05";
    nix2container.url = "github:nlewo/nix2container";
  };

  outputs = { self, nixpkgs, nix2container }: let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
    nix2containerPkgs = nix2container.packages.x86_64-linux;
  in {
    ########################################
    # Docker image for website compilation #
    ########################################
    # Create the image with: nix run .#container.copyToDockerDaemon
    packages.x86_64-linux.container = nix2containerPkgs.nix2container.buildImage {
      name = "wentasah/osy-pages";
      tag = "latest";
      copyToRoot = [
        (pkgs.buildEnv {
          name = "root";
          paths = with pkgs; [ hugo busybox doxygen ];
          pathsToLink = [ "/bin" ];
        })
      ];
      config = {
        Cmd = [ "/bin/sh" ];
      };
    };
  };
}
