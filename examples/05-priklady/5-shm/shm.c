#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
    char *shared_mem;
    int fd = shm_open("pamet", O_RDWR | O_CREAT | O_TRUNC, 0666);
    ftruncate(fd, 10000);
    shared_mem = mmap(NULL, 10000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);
    for (int i = 0; i < 1000; i++) {
        sprintf(shared_mem, "Data %03i\n", i);
        shared_mem[9] = 0;
        shared_mem += 10;
    }
    return 0;
}
