#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

// druhy proces
int main()
{
    char line[1024];
    int fd = open("/tmp/myfifo", O_RDONLY, 0), rd;
    while ((rd = read(fd, line, 1000)) > 0) {
        line[rd] = 0;
        printf("Prijata data: %i %s\n", rd, line);
    }
    return 0;
}
