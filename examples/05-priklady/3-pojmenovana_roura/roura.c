#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

// prvni proces
int main()
{
    char line[1000];
    mknod("/tmp/myfifo", S_IFIFO | 0660, 0);
    int fd = open("/tmp/myfifo", O_WRONLY, 0);
    for (int i = 0; i < 100000; i++) {
        sprintf(line, "Data %i\n", i);
        write(fd, line, strlen(line));
    }
    return 0;
}
